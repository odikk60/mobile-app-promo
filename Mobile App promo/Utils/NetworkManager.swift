//
//  NetworkManager.swift
//  Mobile App promo
//
//  Created by Ariesta APP on 23/01/24.
//

import Foundation
import Alamofire

struct EmptyRequest: Codable {
    
}

class NetworkManager {
    static let shared = NetworkManager()
    private static let baseURL = "http://demo5853970.mockable.io/"
    private static let apiKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjc1OTE0MTUwLCJleHAiOjE2Nzg1MDYxNTB9.TcIgL5CDZYg9o8CUsSjUbbUdsYSaLutOWni88ZBs9S8"
    private let imageCache = NSCache<NSString, UIImage>()
    
    func fetchData<T: Codable, U: Codable> (endpoint: String, method: HTTPMethod, requestData: T? = EmptyRequest(), responseType: U.Type, completion: @escaping (U?, Int?, Error?) -> Void) {
        guard let url = URL(string: NetworkManager.baseURL + endpoint) else {return}
        let header: HTTPHeaders = HTTPHeaders([
            HTTPHeader(name: "Authorization", value: "Bearer \(NetworkManager.apiKey)")
        ])
            AF.request(
                url,
                method: method,
                parameters: nil,
                encoding: JSONEncoding.default,
                headers: header
            ).responseDecodable(of: responseType) { (response) in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        guard let data = response.data else {return}
                        self.decodeData(response: data, responseType: responseType) { resp, err in
                            completion(resp, response.response?.statusCode, err)
                        }
                    }
                case .failure(let failure):
                    print("Error Response: \(failure)")
                    completion(nil, nil, failure)
                }
        }
        
    }
    
    private func decodeData<T: Codable>(response: Data, responseType: T.Type, completion: @escaping (T?, Error?) -> Void) {
        do {
            let decodedData = try JSONDecoder().decode(responseType, from: response)
            completion(decodedData, nil)
        } catch(let err) {
            print("Error Decode: \(err)")
            completion(nil, err)
        }
    }
    
    func downloadImage(_ urlString: String, completion: @escaping (UIImage?, Error?) -> Void){
        if let cache = imageCache.object(forKey: urlString as NSString) {
            completion(cache, nil)
            return
        }
        
        guard let url = URL(string: urlString) else {return}
        var img = UIImage()
        URLSession.shared.dataTask(with: url) { data, _, err in
            if let data {
                let compressedImage = UIImage(data: data)?.jpegData(compressionQuality: 0.5)
                img = UIImage(data: (compressedImage ?? data)) ?? UIImage()
                
                self.imageCache.setObject(img, forKey: urlString as NSString)
                completion(img, nil)
                return
            }
            completion(nil, err)
            return
        }.resume()
        
    }
}
