//
//  PromoCell.swift
//  Mobile App promo
//
//  Created by Ariesta APP on 23/01/24.
//

import UIKit

class PromoCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    
    var nib: UINib = UINib(nibName: "promoCell", bundle: nil)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    private func setupUI() {
        titleLabel.font = .boldSystemFont(ofSize: 24)
        titleLabel.numberOfLines = 0
        titleLabel.textColor = .white
        mainImageView.contentMode = .scaleAspectFill
        gradientView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
        gradientView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            gradientView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor),
            gradientView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            gradientView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
            gradientView.heightAnchor.constraint(equalToConstant: mainView.frame.height/2)
        ])
    }

}
