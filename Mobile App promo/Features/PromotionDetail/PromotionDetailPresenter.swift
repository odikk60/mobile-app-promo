//
//  PromotionDetailPresenter.swift
//  Mobile App promo
//
//  Created by Ariesta APP on 24/01/24.
//

import Foundation

class PromotionDetailPresenter {
    
    private var promotionData: PromoEntity
    init(promotionData: PromoEntity) {
        self.promotionData = promotionData
    }
    func getPromotionData() -> PromoEntity {
        return promotionData
    }
    
    
}
