//
//  PromotionDetailViewController.swift
//  Mobile App promo
//
//  Created by Ariesta APP on 24/01/24.
//

import UIKit
import WebKit
class PromotionDetailViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    private var presenter: PromotionDetailPresenter?
    init(presenter: PromotionDetailPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    private func setupUI() {
        title = presenter?.getPromotionData().name
        if let url = URL(string: presenter?.getPromotionData().detail ?? "") {
            webView.load(URLRequest(url: url))
        }
        
    }
}
