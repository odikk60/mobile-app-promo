//
//  HomeInteractor.swift
//  Mobile App promo
//
//  Created by Ariesta APP on 23/01/24.
//

import Foundation

protocol HomeUseCase {
    func fetchPromoData(completion: @escaping (PromoModel?, Error?) -> Void)
    func mapPromoModelToEntity(data: PromoModel?) -> HomeEntity
}

class HomeInteractor: HomeUseCase {
    static let shared = HomeInteractor()
    func fetchPromoData(completion: @escaping (PromoModel?, Error?) -> Void) {
        NetworkManager.shared.fetchData(endpoint: "promos", method: .get, responseType: PromoModel.self) { resp, code, err in
            if let resp {
                completion(resp, nil)
            }
        }
    }
    func mapPromoModelToEntity(data: PromoModel?) -> HomeEntity {
        var promoEntities: [PromoEntity] = []
        _ = data?.promos.map({item in
            let entity = PromoEntity(id: item?.id, name: item?.name, imageUrl: item?.imageUrl, detail: item?.detail)
            promoEntities.append(entity)
        })
        
        
        return HomeEntity(promo: promoEntities)
    }
}
