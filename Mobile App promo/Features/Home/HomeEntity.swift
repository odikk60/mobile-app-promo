//
//  HomeEntity.swift
//  Mobile App promo
//
//  Created by Ariesta APP on 23/01/24.
//

import Foundation

/**
 {
 "promos": [
     {
         "id": 1,
         "name": "BNI Mobile Banking",
         "images_url": "https://www.bni.co.id/Portals/1/BNI/Beranda/Images/Beranda-MobileBanking-01-M-Banking1.png",
         "detail": "https://www.bni.co.id/id-id/individu/simulasi/bni-deposito"
     },
     {
         "id": 2,
         "name": "BNI Wholesale",
         "images_url": "https://bit.ly/MarcommBNIFleksi-2023",
         "detail": "https://www.bni.co.id/id-id/korporasi/solusi-wholesale/tentang-kami"
     }
 ]
}
**/

struct PromoModel: Codable {
    let promos: [PromoDetail?]
}

struct PromoDetail: Codable {
    let id: Int?
    let name: String?
    let imageUrl: String?
    let detail: String?
    
    private enum CodingKeys: String, CodingKey {
        case id, name, detail
        case imageUrl = "images_url"
    }
}

struct HomeEntity {
    let promo: [PromoEntity?]
}

struct PromoEntity {
    let id: Int?
    let name: String?
    let imageUrl: String?
    let detail: String?
}
