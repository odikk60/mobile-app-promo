//
//  HomeRouter.swift
//  Mobile App promo
//
//  Created by Ariesta APP on 23/01/24.
//

import UIKit

class HomeRouter {
    static let shared = HomeRouter()
    
    func goToHome() -> HomeViewController {
        let presenter = HomePresenter(router: HomeRouter.shared, interactor: HomeInteractor.shared)
        let vc = HomeViewController(presenter: presenter)
        return vc
    }
    
    func goToDetail(nav: UINavigationController, data: PromoEntity) {
        let presenter = PromotionDetailPresenter(promotionData: data)
        let vc = PromotionDetailViewController(presenter: presenter)
        nav.pushViewController(vc, animated: true)
        
    }
}
