//
//  HomeViewController.swift
//  Mobile App promo
//
//  Created by Ariesta APP on 23/01/24.
//

import UIKit

class HomeViewController: UIViewController {
    private var presenter: HomePresenter?
    
    @IBOutlet weak var promoCollectionView: UICollectionView!
    @IBOutlet weak var emptyDataLabel: UILabel!
    private var isLoadingPromotion = true
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(presenter: HomePresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Promotion"
        setupUI()
        self.presenter?.fetchPromotion() { promotion in
            self.isLoadingPromotion = false
            self.promoCollectionView.reloadData()
            if self.presenter?.getPromotionData()?.promos.count == 0 {
                self.promoCollectionView.isHidden = true
            } else {
                self.promoCollectionView.isHidden = false
                
            }
        }
    }
    private func setupUI() {
        self.emptyDataLabel.text = "No Promotion Available"
        self.promoCollectionView.dataSource = self
        self.promoCollectionView.delegate = self
        self.promoCollectionView.register(UINib(nibName: "PromoCell", bundle: nil), forCellWithReuseIdentifier: "promoCell")
        
    }
}


extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch isLoadingPromotion {
        case true:
            return 1
        default:
            return presenter?.getPromotionData()?.promos.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "promoCell", for: indexPath) as! PromoCell
        cell.layer.cornerRadius = 10
        if isLoadingPromotion {
            cell.titleLabel.text = "..."
        } else {
            let data = presenter?.getPromotionData()?.promos[indexPath.row]
            cell.titleLabel.text = data?.name
            NetworkManager.shared.downloadImage(data?.imageUrl ?? "") { img, err in
                if let img {
                    //handling segmented loading
                    img.prepareForDisplay { [weak self] preparedImage in
                        DispatchQueue.main.async {
                            cell.mainImageView.image = preparedImage
                        }
                    }
                }
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = presenter?.providePromotionList()?.promo[indexPath.row]
        presenter?.routeToDetail(nav: self.navigationController!, data: data ?? PromoEntity(id: nil, name: nil, imageUrl: nil, detail: nil))
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        return CGSize(width: width, height: 150)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
    }
}
