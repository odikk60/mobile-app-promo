//
//  HomePresenter.swift
//  Mobile App promo
//
//  Created by Ariesta APP on 23/01/24.
//

import Foundation
import UIKit

protocol HomePresenterProtocol {
    func fetchPromotion(completion: @escaping ([PromoEntity?]) -> Void)
    func providePromotionList() -> HomeEntity?
    func getPromotionData() -> PromoModel?
    func routeToDetail(nav: UINavigationController, data: PromoEntity)
}

class HomePresenter: HomePresenterProtocol {
    private var router: HomeRouter?
    private var interactor: HomeInteractor?
    private var promotionData: PromoModel?
    init(router: HomeRouter? = nil, interactor: HomeInteractor? = nil) {
        self.router = router
        self.interactor = interactor
    }
    
    func fetchPromotion(completion: @escaping ([PromoEntity?]) -> Void) {
        interactor?.fetchPromoData { resp, err in
            if let resp {
                self.promotionData = resp
                completion(self.providePromotionList()?.promo ?? [])
            }
        }
        
    }
    func getPromotionData() -> PromoModel? {
        return promotionData
    }
    func providePromotionList() -> HomeEntity? {
        return interactor?.mapPromoModelToEntity(data: getPromotionData())
    }
    func routeToDetail(nav: UINavigationController, data: PromoEntity) {
        router?.goToDetail(nav: nav, data: data)
    }
}
