//
//  HomeInteractorTests.swift
//  Mobile App promoTests
//
//  Created by Ariesta APP on 24/01/24.
//

import XCTest
@testable import Mobile_App_promo
final class HomeInteractorTests: XCTestCase {
    var sut: MockHomeInteractor?
    
    override func setUpWithError() throws {
        sut = MockHomeInteractor.shared
    }
    
    override func tearDownWithError() throws {
        sut = nil
    }
    
    func testFetchingPromo() throws {
        sut?.fetchPromoData { data, err in
            XCTAssertNotNil(data?.promos)
            XCTAssertNil(err)
        }
    }
    
    func testMapping() throws {
        let entity = sut?.mapPromoModelToEntity(data: MockPromoModel.promoModel)
        XCTAssertNotNil(entity)
        XCTAssertEqual(entity?.promo[0]?.name, MockPromoModel.promoEntity.name)
    }
}

class MockHomeInteractor: HomeUseCase {
    static let shared = MockHomeInteractor()
    func fetchPromoData(completion: @escaping (PromoModel?, Error?) -> Void) {
        completion(MockPromoModel.promoModel, nil)
    }
    func mapPromoModelToEntity(data: PromoModel?) -> HomeEntity {
        var promoEntities: [PromoEntity] = []
        _ = data?.promos.map({item in
            let entity = PromoEntity(id: item?.id, name: item?.name, imageUrl: item?.imageUrl, detail: item?.detail)
            promoEntities.append(entity)
        })
        
        
        return HomeEntity(promo: promoEntities)
    }
}

struct MockPromoModel {
    static let promoModel = PromoModel(promos: [
        PromoDetail(id: 1, name: "Name", imageUrl: "", detail: "")
    ])
    static let promoDetail = PromoDetail(id: 2, name: "Name", imageUrl: "", detail: "")
    static let promoEntity = PromoEntity(id: 2, name: "Name", imageUrl: "", detail: "")
}
